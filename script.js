document.addEventListener('DOMContentLoaded', () => {
  let selectors = {
    form: document.getElementById('comment'),
    author: document.getElementById('author'),
    message: document.getElementById('message'),
    writeMessageBtn: document.getElementById('_write-message'),
    popup: document.getElementById('popup'),
  }

  let globalData = {
    parentId: false,
  }

  class Message {
    constructor (user) {
      this._id = new Date().getTime();
      this.created_at = new Date();
      this.author = user.author;
      this.message = user.message;
      this.parentId = user.parentId ? user.parentId : null;
    }

    showPopup (popup) {
      popup.style.visibility = 'visible';
    }

    skipMessage (target, parent) {
      target.addEventListener('click', () => {
        parent.remove();
      });
    }

    answerMessage (target, parent) {
      target.addEventListener('click', e => {
        this.showPopup(selectors.popup);
        globalData.parentId = parent.id;
        this.parentId = parent.id;
      });
    }

    getData(date) {
      date = date ? date : new Date();
      const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
      return `${date.getDate()} ${months[date.getMonth()]} ${date.getFullYear()}, ${date.getHours()}:${date.getMinutes()}`
    }

    createBlock (html, className, parent) {
      const newPost = document.createElement('li');
      newPost.classList.add(className);
      newPost.setAttribute('id', this._id)
      newPost.innerHTML = html;
      parent.appendChild(newPost);
      const skipBtn = newPost.querySelector('._skipMessage');
      const answerBtn = newPost.querySelector('._answerMessage');
      this.skipMessage(skipBtn, newPost);
      this.answerMessage(answerBtn, newPost);
    }
    
    renderMessage() {
      console.log(this);
      const answers = document.getElementById('answers');
      const root = answers.querySelector('.message_list');
      const idParentId = this.parentId ? `| Parent ID: #${this.parentId}` : '';
      const newAnswerHTML = `
        <h6 class='message__date'>Posted ${this.getData(this.created_at)} | ID: #${this._id} ${idParentId}</h6>
        <h4 class='message__author'>${this.author}</h4>
        <p class='message__text'>${this.message}</p>
        <input class='_skipMessage' type='button' value='Skip'>
        <input class='_answerMessage' type='button' value='Answer'>
      `;
      this.createBlock(newAnswerHTML, 'answer', root);
    }

    sendAnswer() {
      this.renderMessage(this);
    }
  }

  selectors.writeMessageBtn.addEventListener('click', () => {
    selectors.popup.style.visibility = 'visible';
  });
  
  selectors.form.addEventListener('submit', e => {
    e.preventDefault();
    let message;
    if(globalData.parentId) {
      message = new Message({
        author: selectors.author.value,
        message: selectors.message.value,
        parentId: globalData.parentId,
      });
    } else {
      message = new Message({
        author: selectors.author.value,
        message: selectors.message.value,
      });
    }
    message.sendAnswer();
    selectors.message.value = '';
    selectors.popup.style.visibility = 'hidden';
    globalData.parentId = false;
  });
});